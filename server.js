const bodyParser = require('body-parser')
const cors = require('cors')
const { MongoClient } = require('mongodb');
const app = require('express')()

const port = 4000;
const url = 'mongodb://localhost:27017';
const dbName = 'MessageBoard'
let db;
app.use(bodyParser.json())
app.use(cors())
app.post('/api/message', async (req, res) => {
    const message = req.body
    console.log(req.body);
    db.collection('messages').insertOne(message)
    const foundUser = await db.collection('users').findOne({ name: message.userName });
    console.log(foundUser);
    if (!foundUser) db.collection('users').insertOne({ name: message.userName })
    res.status(200).send()
})

app.get('/api/message', async (req, res) => {
    const docs = await db.collection('messages').find({}).toArray();
    if (!docs) return res.json({ error: "error getting message" })
    res.json(docs)
    console.log(docs)

})




MongoClient.connect(url, function (err, client) {
    if (err) return console.log('mongodb error', err)
    console.log("Connected successfully to server");

    db = client.db(dbName);




});
app.listen(port, () => {
    console.log("raising app")
})
// *********************************//
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose');
const app = require('express')()

const port = 4000;
const url = 'mongodb://localhost:27017/MessageBoard';

app.use(bodyParser.json())
app.use(cors())
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
});
//schema define commen property that message will have 

const Message = mongoose.model('Message', {
    userName: String,
    msg: String
});
const User = mongoose.model('User', {
    name: String,
    messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message' }]
});



app.post('/api/message', async (req, res) => {


    const message = new Message(req.body)
    message.save();
    const user = await User.findOne({ name: message.userName });
    console.log(user);
    if (!user) {
        user = (new User({ name: message.userName })).user.save();
    }
    user.messages.push(message)

    res.status(200).send()
})

app.get('/api/message', async (req, res) => {
    const docs = await Message.find()
    if (!docs) return res.json({ error: "error getting message" })
    res.json(docs)
    console.log(docs)

})
app.get('/api/user/:name', async (req, res) => {
    const name = req.params.name;
    return res.json(await User.findOne({ name }))

})

mongoose.connect(url)




app.listen(port, () => {
    console.log("raising app")
})